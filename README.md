# HTML Scraping and Cleaning

A small, simple, untidy script that scrapes a webserial book for content and then gets it ready for Calibre to create an EPub out of it. I attempted to create the EPub too, but it was not worth the time and effort.

I use the script to scrape it for offline reading, as I sometimes visit relatives that have no internet or cell phone reception.

The process it runs through is:

* Scrapes the Table of Contents of Website (in this case https://tiraas.wordpress.com/table-of-contents/)
* Extracts the contents div of each page
* Adds the title as a \<h2\> with the class "chapter" (for calibre splitting and TOC detection)
* Appends each \<p\> section (as long as they contain no links) to a html file.

Happy with it and it works pretty well. Probaly not the most efficent or fast, but relatively concise.
