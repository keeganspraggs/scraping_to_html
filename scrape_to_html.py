from bs4 import SoupStrainer, BeautifulSoup
import requests
from ebooklib import epub
import re

page_list = []
# pulls the main div that contains the content
only_entry = SoupStrainer("div", { "class" : "entry-content" })

# gets a list of pages from the contents page post on the website
def contentspage(toc):
    contents_page = requests.get(toc)

    entry_content = BeautifulSoup(contents_page.content, "lxml", parse_only=only_entry)
    page_list = []

    for link in entry_content.find_all('a'):
        page_list.append(link.get('href'))

    # removes the last 4 links from the list, as these are social media links
    n = 4
    page_list = page_list[:-n or None]
    return page_list

# creates a html file with titles appended as breaks
def bookcreation(page_list):
    # used to remove any <p></p> tags that contain links as these aren't part of the content
    regex = re.compile(r"[<][a].+[>]")
    for link in page_list:
        page = requests.get(link)
        full_page = BeautifulSoup(page.content, "lxml")
        entry_content = BeautifulSoup(page.content, "lxml", parse_only=only_entry)
        title_name = full_page.title.string

        file = open('./book_files/book.html', 'a')

        file.write('<h2 class=\"chapter\">'+ str(title_name) + '</h2>')

        # searches for <p> tags, skips regexed ones, appends others to file
        for line in entry_content.findAll("p"):
            line_pretty = line.prettify(formatter="html")
            if regex.search(line_pretty):
                continue
            else:
                file.write(line_pretty)

    file.close()
contents_list = contentspage('https://tiraas.wordpress.com/table-of-contents/')
bookcreation(contents_list)
print('Done')
